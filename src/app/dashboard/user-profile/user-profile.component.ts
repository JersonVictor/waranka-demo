import { Component, OnInit } from '@angular/core';
import { Recommended } from '@app/core/models/model.recommended';
import { MatDialog } from '@angular/material';
import { AuthService } from '@app/core/services/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonsService } from '@app/core/services';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  form: FormGroup
  Worker: Recommended;
  loader: boolean = false;
  phone: string;
  user: any;
  submitted: boolean = false;

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private fb: FormBuilder,
    private commonService: CommonsService
  ) { }

  ngOnInit () {
    this.authService.subjectUser.subscribe(user => {
      this.createForm(user);
    });

  }
  controlInput (input) {
    return this.commonService.validateInput(input, this.submitted);
  }

  private createForm (user) {
    this.form = this.fb.group({
      nombres: [user.nombres],
      apellidos: [user.apellidos],
      email: [user.email],
      telefono: [user.telefono],
      edad: [user.edad],
      direccion: [user.direccion || '']
    })
  }


  onSubmit(){
    
  }






}
